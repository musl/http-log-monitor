package main

import (
	"gitlab.com/musl/http-log-monitor/cmd"
)

func main() {
	cmd.Execute()
}
