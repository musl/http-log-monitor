package monitor

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"strconv"
	"sync"
	"time"
)

// Config holds all of the knobs that are needed to monitor a given log
// stream.
type Config struct {
	Source          io.Reader
	SummaryInterval time.Duration
	AlertInterval   time.Duration
	AlertPolicies   []Policy
	CSVHeader       []string
	ArtificialDelay bool
}

// Start reads from the given io.ReadCloser for CSV formatted log lines,
// displays summaries periodically, and alerts on traffic greater than
// the given threshold.
func Start(config Config) {

	wg := sync.WaitGroup{}

	lineChan := make(chan *logLine)
	quitChan := make(chan struct{})

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(lineChan)
		defer close(quitChan)

		csvReader := csv.NewReader(config.Source)
		csvHeader := config.CSVHeader
		var lastLine *logLine

		for {
			line, err := decodeLogLine(csvReader, csvHeader)
			if line == nil && err == nil {
				log.Println("Read a header.")
				continue
			}

			if err == io.EOF {
				log.Println("Reached the end of the stream.")
				return
			}

			if err != nil {
				log.Println(err)
				continue
			}

			// Add a fractional delay for the exercise so status reports print.
			if config.ArtificialDelay && lastLine != nil {
				delay := line.Date.Sub(lastLine.Date)
				if delay > 0 {
					time.Sleep(delay)
				}
			}
			lastLine = line

			lineChan <- line
		}
	}()

	summaryBucketChan := make(chan Bucket)
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(summaryBucketChan)

		policies := config.AlertPolicies

		firstLine := <-lineChan
		bucket := newBucket(firstLine.Date, config.AlertInterval)

		for line := range lineChan {
			// Exclude log lines with dates before the current bucket.
			if line.Date.Before(bucket.Start) {
				continue
			}

			if line.Date.Sub(bucket.Start) > bucket.Interval {
				for i := range policies {
					policies[i].service(bucket, line.Date)
				}

				bucket = newBucket(line.Date, bucket.Interval)
			}

			bucket.update(line)
			summaryBucketChan <- bucket
		}

		log.Println("aggregation routine returning")
	}()

	summaryTicker := time.NewTicker(config.SummaryInterval)
	wg.Add(1)
	go func() {
		defer wg.Done()

		var b Bucket

		for {
			select {
			case <-summaryTicker.C:
				log.Println(b.summary())
			case b = <-summaryBucketChan:
			case <-quitChan:
				return
			}
		}
	}()

	wg.Wait()
}

type logLine struct {
	Host     string
	RFC931   string
	AuthUser string
	Date     time.Time
	Request  string
	Status   int
	Bytes    int
}

func decodeLogLine(r *csv.Reader, header []string) (*logLine, error) {
	record, err := r.Read()
	if err != nil {
		return nil, err
	}

	if record[0] == header[0] {
		return nil, nil
	}

	if len(record) != len(header) {
		return nil, fmt.Errorf("Log line had wrong number of fields: (%d) record: %s", len(record), record)
	}

	requestDateInt, err := strconv.ParseInt(record[3], 10, 64)
	if err != nil {
		return nil, err
	}
	requestDate := time.Unix(requestDateInt, 0)

	requestStatus, err := strconv.Atoi(record[5])
	if err != nil {
		return nil, err
	}

	requestBytes, err := strconv.Atoi(record[6])
	if err != nil {
		return nil, err
	}

	return &logLine{
		Host:     record[0],
		RFC931:   record[1],
		AuthUser: record[2],
		Date:     requestDate,
		Request:  record[4],
		Status:   requestStatus,
		Bytes:    requestBytes,
	}, nil
}

// Bucket holds metrics for a given aggregation window.
type Bucket struct {
	Start       time.Time
	Interval    time.Duration
	LastRequest time.Time
	Metrics     map[string]float64
}

func newBucket(t time.Time, d time.Duration) Bucket {
	return Bucket{
		Start:    t,
		Interval: d,
		Metrics:  make(map[string]float64),
	}
}

func (b *Bucket) update(line *logLine) {
	b.LastRequest = line.Date

	b.Metrics["RequestCount"]++
	b.Metrics["RequestRate"] = b.Metrics["RequestCount"] / (float64(b.Interval) / float64(time.Second))

	if line.Status < 200 || line.Status > 299 {
		b.Metrics["ErrorCount"]++
		b.Metrics["ErrorRate"] = b.Metrics["ErrorCount"] / (float64(b.Interval) / float64(time.Second))
	}
}

func (b *Bucket) summary() string {
	return fmt.Sprintf("Summary for %s: %0.2f req/sec (%0.0f total), %0.2f err/sec (%0.0f total), Last Request: %s",
		b.Start,
		b.Metrics["RequestRate"],
		b.Metrics["RequestCount"],
		b.Metrics["ErrorRate"],
		b.Metrics["ErrorCount"],
		b.LastRequest,
	)
}

type violation struct {
	opened time.Time
}

// Policy is a definition of an alertable condition.
type Policy struct {
	Name      string
	Threshold float64
	Field     string

	violation *violation
}

func (p *Policy) service(b Bucket, t time.Time) {
	if _, ok := b.Metrics[p.Field]; !ok {
		log.Println("Field not found:", p.Field)
		return
	}

	if p.violation == nil && b.Metrics[p.Field] > p.Threshold {
		p.violation = &violation{opened: b.Start}
		log.Println("violation opened:", p.Field, b.Metrics[p.Field], b.Start)
	}

	if p.violation != nil && b.Metrics[p.Field] <= p.Threshold {
		duration := t.Sub(p.violation.opened)
		log.Println("violation closed:", p.Field, duration, b.Start)
		p.violation = nil
	}
}
