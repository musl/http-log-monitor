package monitor

import (
	"encoding/csv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

/*
func init() {
	devnull, _ := os.Open(os.DevNull)
	log.SetOutput(devnull)
}
*/

func TestDecodeLogLine(t *testing.T) {
	expected := &logLine{
		Host:     "0.0.0.0",
		RFC931:   "-",
		AuthUser: "apache",
		Date:     time.Unix(1549574332, 0),
		Request:  "GET /status/check",
		Status:   200,
		Bytes:    512,
	}

	result, err := decodeLogLine(
		csv.NewReader(
			strings.NewReader(
				`"0.0.0.0","-","apache",1549574332,"GET /status/check",200,512`,
			)),
		[]string{"remotehost", "rfc931", "authuser", "date", "request", "status", "bytes"},
	)

	if assert.NoError(t, err) {
		assert.Equal(t, expected, result)
	}
}

func TestBucketupdate(t *testing.T) {
	now := time.Now()

	reqLine := &logLine{
		Host:     "0.0.0.0",
		RFC931:   "-",
		AuthUser: "apache",
		Date:     now.Add(10 * time.Millisecond),
		Request:  "GET /status/check",
		Status:   200,
		Bytes:    512,
	}

	errLine := &logLine{
		Host:     "0.0.0.0",
		RFC931:   "-",
		AuthUser: "apache",
		Date:     now.Add(20 * time.Millisecond),
		Request:  "GET /status/check",
		Status:   500,
		Bytes:    512,
	}

	bucket := newBucket(now, 1*time.Second)

	bucket.update(reqLine)
	assert.Equal(t, 1.0, bucket.Metrics["RequestCount"])
	assert.Equal(t, 1.0, bucket.Metrics["RequestRate"])

	bucket.update(errLine)
	assert.Equal(t, 1.0, bucket.Metrics["ErrorCount"])
	assert.Equal(t, 1.0, bucket.Metrics["ErrorRate"])
}

func TestPolicyService(t *testing.T) {
	now := time.Now()
	policy := Policy{Name: "Test Policy", Field: "Test", Threshold: 1}
	bucket := newBucket(now.Add(-1*time.Second), 2*time.Second)

	bucket.Metrics["Test"] = 2
	policy.service(bucket, now)
	assert.NotNil(t, policy.violation)

	bucket.Metrics["Test"] = 0
	policy.service(bucket, now)
	assert.Nil(t, policy.violation)
}
