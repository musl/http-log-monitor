# http-log-monitor

A take-home-project for my application at DataDog. This go program reads CSV formatted logs in on standard input and outputs a log of its own. It periodically prints summaries and when traffic crosses a configurable threshold, opens and closes alert violations.

## Prerequisites

- go 1.12+
- gnu make

## Building

1. Running `make` will run the tests and build the binary.

### Example build, test output:

```
$ make
rm -f http-log-monitor
go clean .
go test -v ./...
?       gitlab.com/musl/http-log-monitor        [no test files]
?       gitlab.com/musl/http-log-monitor/cmd    [no test files]
=== RUN   TestDecodeLogLine
--- PASS: TestDecodeLogLine (0.00s)
=== RUN   TestBucketupdate
--- PASS: TestBucketupdate (0.00s)
=== RUN   TestPolicyService
2020/07/17 10:06:23 violation opened: Test 2 2020-07-17 10:06:22.6680726 -0700 PDT m=-0.999436499
2020/07/17 10:06:23 violation closed: Test 1s 2020-07-17 10:06:22.6680726 -0700 PDT m=-0.999436499
--- PASS: TestPolicyService (0.00s)
PASS
ok      gitlab.com/musl/http-log-monitor/lib/monitor    (cached)
go build -o http-log-monitor -ldflags '-X github.com/musl/http-log-monitor/cmd.Version=v0.0.0 -X github.com/musl/http-log-monitor/cmd.Revision=NONE' .
$
```

## Usage

`cat sample.csv | ./http-log-monitor -x`

### Expected output:

```
$ cat sample.csv | ./http-log-monitor
http-log-monitor 2020/07/17 10:14:42 monitor.go:47: Read a header.
http-log-monitor 2020/07/17 10:14:42 monitor.go:238: violation opened: RequestRate 13.483333333333333 2019-02-07 13:11:00 -0800 PST
http-log-monitor 2020/07/17 10:14:42 monitor.go:243: violation closed: RequestRate 4m3s 2019-02-07 13:13:01 -0800 PST
http-log-monitor 2020/07/17 10:14:42 monitor.go:238: violation opened: RequestRate 21.733333333333334 2019-02-07 13:15:03 -0800 PST
http-log-monitor 2020/07/17 10:14:42 monitor.go:52: Reached the end of the stream.
http-log-monitor 2020/07/17 10:14:42 monitor.go:103: aggregation routine returning
$
```

## Project Notes

- The log input does not correspond with realtime. I added an optional delay to allow it to function as if piping a CSV file would behave as if the log lines were being piped in in real time.
- This challenge shows an example of separating different tasks (decoding, evaluation, and reporting) into different goroutines. This program could be further improved if decoding is a bottleneck by fanning the log lines in or splitting up the program into distinct daemons with queues inbetween them.
- The code was broken up in to moderate chunks for testability, but there's further work to be done there to increase coverage and to improve the testability of the code. Time was the largest impediment here.

