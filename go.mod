module gitlab.com/musl/http-log-monitor

go 1.13

require (
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.2.2
)
