package cmd

import (
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/musl/http-log-monitor/lib/monitor"
)

var (
	// AppName is the identifier used for configuration and log
	// messages.
	AppName = `http-log-monitor`

	// Version is the sematic version string of this app, set by the
	// build system.
	Version = ``

	// Revision is the id of the changeset from the version control
	// system, set by the build system.
	Revision = ``

	config = monitor.Config{
		Source: os.Stdin,
		AlertPolicies: []monitor.Policy{
			{Name: "Request Rate", Field: "RequestRate", Threshold: 10},
		},
		CSVHeader: []string{"remotehost", "rfc931", "authuser", "date", "request", "status", "bytes"},
	}

	rootCmd = &cobra.Command{
		Use:   AppName,
		Short: `A log monitor for streaming CSV httpd logs.`,
		Long:  `A log monitor for streaming CSV httpd logs to periodically print summaries and alert on trafic thresholds.`,
		Run: func(cmd *cobra.Command, args []string) {
			monitor.Start(config)
		},
	}
)

func init() {
	rootCmd.Flags().DurationVarP(&(config.SummaryInterval), "summary", "s", 10*time.Second, "Time between summary reports")
	rootCmd.Flags().DurationVarP(&(config.AlertInterval), "alert", "a", 2*time.Minute, "Alert aggregation window size")
	rootCmd.Flags().BoolVarP(&(config.ArtificialDelay), "example", "x", false, "Add artificial delay based on the timestamps of each record for the example")
}

// Execute sets up anything needed at runtime before the root command is
// dispatched and then dispatches it.
func Execute() error {
	rand.Seed(time.Now().UnixNano())

	log.SetOutput(os.Stderr)
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	log.SetPrefix(AppName + ` `)

	return rootCmd.Execute()
}
